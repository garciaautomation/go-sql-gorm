package gorm

import (
	"fmt"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

// User func
type User struct {
	gorm.Model
	Name     string
	Password []byte
	Email    string `gorm:"unique"`
}

// UserSubset s
type UserSubset struct {
	Name string
	// Password []byte
	Email string
}

var (
	err error
	db  *gorm.DB
)

// InitialMigration func
func InitialMigration() {
	fmt.Println(dbConn)
	db, err = gorm.Open("mysql", dbConn)
	if err != nil {
		fmt.Println("Connection Failed")
		fmt.Print(err.Error())
	}
	defer db.Close()
	db.AutoMigrate(&User{})
}

// PrintStuff f
func PrintStuff() {
	fmt.Println("STUFF")
}

// VerifyUserByEmail f
func VerifyUserByEmail(email string) bool {
	db, err := gorm.Open("mysql", dbConn)
	if err != nil {
		fmt.Print(err.Error())
	}
	defer db.Close()
	var user User
	user.Email = email
	if err = db.Where("email = ?", email).Find(&user).Error; err != nil {
		fmt.Println("No record for user: " + email + " exist, and therefore cannot be updated")
		return false
	}
	return true

}

// GetUsers func
func GetUsers(w http.ResponseWriter, r *http.Request) []UserSubset {
	db, err := gorm.Open("mysql", dbConn)
	if err != nil {
		// Assume table doesn't exist
	}

	defer db.Close()
	var users []User
	db.Find(&users)
	var us UserSubset
	var u []UserSubset
	for _, v := range users {
		us.Email = v.Email
		us.Name = v.Name
		// us = &us{Name: v.Name, Email: v.Email}
		u = append(u, us)
	}

	// json.NewEncoder(w).Encode(users)
	// j, _ := json.Marshal(users)
	// fmt.Fprint(w, "All users endpoint reached\n")
	return u
}

// NewUser func
// func NewUser(w http.ResponseWriter, r *http.Request) {
func NewUser(name string, email string, password []byte) {
	db, err := gorm.Open("mysql", dbConn)
	if err != nil {
		fmt.Print(err.Error())
	}
	defer db.Close()

	// vars := mux.Vars(r)
	// name := vars["name"]
	// email := vars["email"]
	db.Create(&User{Name: name, Email: email, Password: password})
	fmt.Printf("Created New User: " + name + " email: " + email + "\n")
}

// DeleteUser func
func DeleteUser(w http.ResponseWriter, r *http.Request) {
	db, err := gorm.Open("mysql", dbConn)
	if err != nil {
		fmt.Print(err.Error())
	}
	defer db.Close()

	vars := mux.Vars(r)
	email := vars["email"]
	fmt.Println(email)
	var user User
	db.Where("email = ?", email).Find(&user)
	fmt.Print(&user)

	fmt.Fprintf(w, "Deleted User: "+email+"\n")
}

// UpdateUser func
func UpdateUser(w http.ResponseWriter, r *http.Request) {
	db, err := gorm.Open("mysql", dbConn)
	if err != nil {
		fmt.Print(err.Error())
	}
	defer db.Close()

	vars := mux.Vars(r)
	email := vars["email"]
	name := vars["name"]

	var user User
	if err = db.Where("email = ?", email).Find(&user).Error; err != nil {
		fmt.Println("No record for email: " + email + " exist, and therefore cannot be updated")
	} else {
		user.Name = name
		fmt.Print(&user)
		db.Save(&user)
		fmt.Fprintf(w, "Updated User: "+email+"\n")
	}
}

// GetUserPasswordHash func
func GetUserPasswordHash(w http.ResponseWriter, r *http.Request) ([]byte, error) {
	db, err := gorm.Open("mysql", dbConn)
	if err != nil {
		fmt.Print(err.Error())
	}
	defer db.Close()
	var user User
	r.ParseForm()
	email := r.Form["email"][0]
	if err = db.Where("email = ?", email).Find(&user).Error; err != nil {
		fmt.Println("No record for user: " + email + " exist, and therefore cannot be updated")
	}
	passsword := user.Password
	return passsword, err
}
