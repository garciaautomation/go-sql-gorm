package gorm

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

var (
	// parseTime to solve "Scan error...unsupported Scan...Value type []uint8 into type *time.Time
	dbUser   string = "adrian"
	dbAddres string = "@tcp(127.0.0.1:3306)/"
	dbName   string = "test"
	dbOpts   string = "?parseTime=true"
	dbConn          = dbUser + dbAddres + dbName + dbOpts
)

func landingPage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "Whazzup!")
}

func handleRequests() {
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/", landingPage).Methods("GET")
	// router.HandleFunc("/users", GetUsers).Methods("GET")
	// router.HandleFunc("/users/{name}/{email}", NewUser).Methods("POST")
	router.HandleFunc("/users/{name}/{email}", DeleteUser).Methods("DELETE")
	router.HandleFunc("/users/{name}/{email}", UpdateUser).Methods("PUT")
	fmt.Println("Server started on port 8080")
	log.Fatal(http.ListenAndServe(":8080", router))
}
